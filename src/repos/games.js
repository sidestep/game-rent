import fetchJsonp from '../utils/fetch_jsonp.js'

const limit = 100
const api_key ='5e57f67f91f9ea33558f01628a3d393ea9ebfd8a'
const api_url = `https://www.giantbomb.com/api/games/?format=jsonp&field_list=name,image&limit=${limit}&api_key=${api_key}&filter=name:`

export async function search(term, timeout)
{
    const resp = await fetchJsonp(api_url+term, {jsonpCallback: 'json_callback', timeout: timeout})
    if(resp.ok)
    {
        const json = await resp.json()
        let games = json.results.map((g) => {return {title: g.name, icon: g.image.icon_url}})
        return Promise.resolve(games)
    }
    else
    {
        return Promise.reject(`HTTP error. Status code: ${resp.status}`)
    }
}

export default { search }
