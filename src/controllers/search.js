import gameView from '../views/game.js'
// use mock implementations to test error conditions
//import {search as searchGames} from '../repos/timeoutMockGames.js'
//import {search as searchGames} from '../repos/mockGames.js'
import {search as searchGames} from '../repos/games.js'
import {add as addToBasket, count as countBasket} from '../repos/basket.js'
import {htmlToDom} from '../utils/dom.js';

const searchTimeout = 5000

function onTryGame(gameId, gameData)
{
    addToBasket(gameId, gameData)
        .then((cnt)=>{itemCount.textContent = cnt});
}

function selectableGameViev(game)
{
    let dom = htmlToDom( 
        `<div class="selectGame">
           ${gameView(game)}
           <button type="button" class="btn">try it!</button>
        </div>`).firstElementChild
    let button = dom.lastElementChild
    button.addEventListener('click', function(e){ onTryGame(game.title, game.icon); e.target.disabled = true; }) 
    return dom;
}

function renderGames(games)
{
    if(!games?.length)
    {
        gameList.innerHTML = "No games found - try different search term."
        return;
    }
    gameList.innerHTML = ''
    games.forEach((g)=> { gameList.append(selectableGameViev(g)) })
}

export default function onPageLoaded()
{
    countBasket().then((c)=>{itemCount.textContent = c}) 
    txtGameSearch.onkeydown = async function onEnter(e) 
    {
        let term = e.target.value.trim()
        if(e.key === 'Enter' && term) 
        {
            gameList.innerHTML = 'Searching...'
            searchGames(term, searchTimeout).then(renderGames).
                catch((e) => 
                    {
                        gameList.innerHTML = 'Error, see console log for details'
                        throw e
                    })
        }
    }
}
