export default function gameView(game)
{
    return `<div class="aGame">
                <img class="gameIcon" alt='${game.title}' title='${game.title}' src="${game.icon}" />
                <span class="gameTitle">${game.title}</span>
            </div>`
}

