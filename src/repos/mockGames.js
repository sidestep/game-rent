function genColor()
{
    return "#" + Math.floor(Math.random() *  0xFFFFFF).
        toString(16).
        padStart(6, 0)
}

export function search(term, timeout)
{
    let games = []
    for(let i=0; i<100; ++i)
    {
        games.push(
            {
                icon:`data:image/svg+xml;utf8,<svg height='100' width='100'><circle cx='50' cy='50' r='40' fill='${genColor()}' stroke='green' stroke-width='3'/></svg>`,
                title:`${term} game ${i}`
            })
    }
    return Promise.resolve(games)
}

export default { search }
