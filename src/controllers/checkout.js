import gameView from '../views/game.js'
import {getAll as getBasketItems, clear as clearBasket} from '../repos/basket.js'
import {htmlToDom} from '../utils/dom.js'

export default function onPageLoaded()
{
   btnCancel.addEventListener('click', function() { clearBasket().then(()=> {window.location.replace('index.html')}) })
    getBasketItems().
        then((games) => 
            {
                for (let [k, v] of Object.entries(games))
                {
                    gameList.appendChild(htmlToDom(gameView({title:k, icon:v})))
                }
            })
}
