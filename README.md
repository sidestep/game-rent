# Game rental

## TBD

- Media rules for mobile clients
- Edit basket functionality
- Error handling
- Paging?

### Hosted on: https://sidestep.gitlab.io/game-rent/ 

### To host on your machine:
1. Run

```
sudo python -m http.server 80  > /dev/null 2>&1 &
```
2. Navigate to http://localhost/

