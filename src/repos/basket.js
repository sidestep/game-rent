
export function add(key, data)
{
   return Promise.resolve(
        {
            then: function(resolve)
                {
                    localStorage.setItem(key, data);
                    resolve(localStorage.length);
                }
        });
}

export function getAll()
{
   return Promise.resolve(
        {
            then: function(resolve)
                {
                    resolve({ ...localStorage })
                }
        });
}

export function clear()
{
   return Promise.resolve(
        {
            then: function(resolve)
                {
                    localStorage.clear()
                    resolve(localStorage.length);
                }
        });
}

export function count()
{
   return Promise.resolve(
        {
            then: function(resolve)
                {
                    resolve(localStorage.length);
                }
        });
}
