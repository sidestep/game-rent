export function htmlToDom(html)
{
    return document.createRange().createContextualFragment(html)
}
